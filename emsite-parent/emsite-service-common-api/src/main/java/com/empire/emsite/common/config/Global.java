/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.common.config;

//import com.ckfinder.connector.ServletContextFactory;

/**
 * 类Global.java的实现描述：全局配置类
 * 
 * @author arron 2017年10月30日 下午3:51:28
 */
public class Global {
    /**
     * 默认返回数据库类型为mysql, 如果需要修改，**需要在子项目中覆盖此方法
     * 
     * @return
     */
    public static String getJdbcType() {
        return "mysql";
    }
}
